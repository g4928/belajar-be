fn main() {

}
// use async_graphql::*;
// use serde::{Deserialize, Serialize};
// use sqlx::postgres::PgPool;
// use uuid::Uuid;
//
// use crate::{
//     models::data_users::{NewUserModel, UserModel},
//     models::santri::{NewSantriModel, SantriModel},
//     models::person::{NewPersonModel, PesonModel},
//     database::repository,
//     database::get_person
// };
// // use crate::models::data_users::{NewUserModel, UserModel};
// // use crate::models::santri::{NewSantriModel, SantriModel};
// // use crate::models::person::{NewPersonModel, PesonModel};
// // use crate::database::repository;
// // use crate::database::get_person;
//
// pub type AppSchema = Schema<QueryRoot, MutationRoot, EmptySubscription>;
//
// #[derive(SimpleObject)]
// struct User {
//     id: i32,
//     nama_siswa: String,
//     username: Option<String>
// }
//
// impl From<&UserModel> for User {
//     fn from(model : &UserModel) -> Self {
//         User {
//             id: model.id.clone(),
//             nama_siswa: model.nama_siswa.clone(),
//             username: model.username.clone()
//         }
//     }
// }
//
// #[derive(InputObject)]
// pub struct UsersInput {
//     id: i32,
//     nama_siswa: String,
//     username: Option<String>
// }
//
// #[derive(SimpleObject)]
// struct Santri {
//     id: i32,
//     nama_santri: String,
//     password: String
// }
//
// impl From<&SantriModel> for Santri {
//     fn from(model : &SantriModel) -> Self {
//         Santri {
//             id: model.id.clone(),
//             nama_santri: model.nama_santri.clone(),
//             password: model.password.clone()
//         }
//     }
// }
//
// #[derive(SimpleObject)]
// struct Person {
//     pub id: Uuid,
//     pub nama_siswa: String,
//     pub username: Option<String>,
//     pub password: Option<String>
// }
//
// impl From<&PesonModel> for Person {
//     fn from(model : &PesonModel) -> Self {
//         Person {
//             id: model.id.clone(),
//             nama_siswa: model.nama_siswa.clone(),
//             username: model.username.clone(),
//             password: model.password.clone()
//         }
//     }
// }
//
// #[derive(InputObject)]
// pub struct PersonInput {
//     pub id: Uuid,
//     pub nama_siswa: String,
//     pub username: Option<String>,
//     pub password: Option<String>
// }
//
// pub struct QueryRoot;
//
// #[Object]
// impl QueryRoot {
//     async fn get_users(&self, ctx: &Context<'_>) -> Vec<User> {
//         repository::get_all(ctx.data::<PgPool>().expect("Can't get database connection"))
//             .await
//             .unwrap()
//             .iter()
//             .map(User::from)
//             .collect()
//     }
//
//     async fn get_users_by_id(&self, ctx: &Context<'_>, id: i32) -> Vec<User> {
//         repository::get_users_by_id(id,ctx.data::<PgPool>().expect("Can't get database connection"))
//             .await
//             .unwrap()
//             .iter()
//             .map(User::from)
//             .collect()
//     }
//
//     async fn get_all_santri(&self, ctx: &Context<'_>) -> Vec<Santri> {
//         repository::get_all_santri(ctx.data::<PgPool>().expect("Can't get database connection"))
//             .await
//             .unwrap()
//             .iter()
//             .map(Santri::from)
//             .collect()
//     }
//
//     async fn get_all_person(&self, ctx: &Context<'_>) -> Vec<Person> {
//         get_person::get_all(ctx.data::<PgPool>().expect("Can't get database connection"))
//             .await
//             .unwrap()
//             .iter()
//             .map(Person::from)
//             .collect()
//     }
// }
//
// pub struct MutationRoot;
//
// #[Object]
// impl MutationRoot {
//     async fn check(&self, ctx: &Context<'_>) -> bool {
//         true
//     }
//
//     async fn input_user(&self, ctx: &Context<'_>, user: UsersInput) -> User {
//         let new_user = NewUserModel {
//             id: user.id,
//             nama_siswa: user.nama_siswa,
//             username: user.username
//         };
//
//         let input_user = repository::insert(new_user, ctx.data::<PgPool>().expect("Can't get database connection"))
//             .await
//             .expect("Can't create user");
//
//         User::from(&input_user)
//     }
//
//     async fn update_user(&self, ctx: &Context<'_>, user: UsersInput) -> Result<String, Error> {
//
//         let up_user = UserModel {
//             id: user.id,
//             nama_siswa: user.nama_siswa,
//             username: user.username
//         };
//
//         let updated = repository::update_user(up_user, ctx.data::<PgPool>()
//             .expect("Can't get database connection"))
//             .await
//             .unwrap();
//
//         if updated.rows_affected() > 0 {
//             return Ok(String::from("user telah diupdate"));
//         }
//
//         Err(Error::new("Can't update a users"))
//     }
//
//     async fn delete_by_id(&self, ctx: &Context<'_>, id: i32) -> Result<String, Error> {
//
//         let deleted = repository::delete_by_id(id, ctx.data::<PgPool>()
//             .expect("Can't get database connection"))
//             .await
//             .unwrap();
//
//         if deleted.rows_affected() > 0 {
//             return Ok(String::from("user telah dihapus"));
//         }
//
//         Err(Error::new("Can't delete a users"))
//     }
// }
//
