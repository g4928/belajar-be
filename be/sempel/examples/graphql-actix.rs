use actix_web::{ App, web, HttpServer, HttpRequest, HttpResponse, Result };
use async_graphql::*;
use async_graphql_actix_web::{GraphQLRequest, GraphQLResponse};
use async_graphql::http::{playground_source, GraphQLPlaygroundConfig};
// use async_graphql::dataloader::DataLoader;
// use serde::{Deserialize, Serialize};
use sqlx::{postgres::PgPoolOptions, PgPool};
use sqlx::postgres::PgQueryResult;


//-------------------------------------------------
fn configure_service(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/")
            .route(web::post().to(index))
            .route(web::get().to(index_playgroud))
    );
}


async fn index(
    schema: web::Data<AppSchema>,
    _http_req: HttpRequest,
    req: GraphQLRequest
) -> GraphQLResponse {
    let query = req.into_inner();

    schema.execute(query).await.into()

}

async fn index_playgroud() -> Result<HttpResponse> {
    let source = playground_source(GraphQLPlaygroundConfig::new("/").subscription_endpoint("/"));
    Ok(HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(source))
}

pub type AppSchema = Schema<QueryRoot, MutationRoot, EmptySubscription>;

#[derive(SimpleObject)]
pub struct Users {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}

impl From<&UserEntity> for Users {
    fn from(entity: &UserEntity) -> Self {
        Users {
            id: entity.id.clone(),
            nama_siswa: entity.nama_siswa.clone(),
            username: entity.username.clone()
        }
    }
}

#[derive(InputObject)]
pub struct UsersInput {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}

pub struct UserEntity {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}

pub struct NewUserEntity {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}


//-------------------------------------------------------------------------------------

//get data
pub async fn get_all(pool: &PgPool) -> Result<Vec<UserEntity>, Error> {
    let users = sqlx::query_as!(UserEntity, "SELECT id, nama_siswa, username FROM users")
        .fetch_all(pool)
        .await?;

    Ok(users)
}

pub async fn get_by_id(id: i32, pool: &PgPool) -> Result<Vec<UserEntity>, Error> {
    let users = sqlx::query_as!(UserEntity, "SELECT id, nama_siswa, username FROM users WHERE id = $1",
    id )
        .fetch_all(pool)
        .await?;

    Ok(users)
}

//input data
async fn insert(new_user: NewUserEntity, pool: &PgPool) -> Result<UserEntity, Error> {
    let rec  = sqlx::query!( "INSERT INTO users (id, nama_siswa, username) VALUES ($1, $2, $3)
        RETURNING id, nama_siswa, username",
        new_user.id, new_user.nama_siswa, new_user.username )
        .fetch_one(pool)
        .await?;

    let input = UserEntity {
        id: rec.id,
        nama_siswa: rec.nama_siswa,
        username: rec.username
    };

    Ok( input )
}

//delete
pub async fn delete(nama_siswa: String, pool: &PgPool) -> Result<PgQueryResult, sqlx::Error> {
    let del = sqlx::query!("DELETE FROM users WHERE nama_siswa = $1", nama_siswa)
        .execute(pool)
        .await?;

    Ok(del)
}

pub async fn delete_by_id(id: i32, pool: &PgPool) -> Result<PgQueryResult, sqlx::Error> {
    let del = sqlx::query!("DELETE FROM users WHERE id = $1", id)
        .execute(pool)
        .await?;

    Ok(del)
}

//update
pub async fn update(id: i32, nama_siswa: String, username: Option<String>, pool: &PgPool)  -> Result<PgQueryResult, sqlx::Error> {
    sqlx::query!("UPDATE users SET nama_siswa = $1, username = $2 WHERE id = $3", nama_siswa, username, id)
        .execute(pool)
        .await
}

pub async fn update_data(up: Users, pool: &PgPool)  -> Result<PgQueryResult, sqlx::Error> {
    sqlx::query!("UPDATE users SET nama_siswa = $1, username = $2 WHERE id = $3", up.nama_siswa, up.username, up.id)
        .execute(pool)
        .await
}

//-------------------------------------------------------------------------------------

pub struct QueryRoot;

#[Object]
impl QueryRoot {
    async fn healt_check(&self, _ctx: &Context<'_>) -> bool {
        true
    }
    // async fn users(&self, ctx: &Context<'_>) -> Vec<Users> {
    //     let pool = ctx.data::<PgPool>().unwrap();
    //     sqlx::query_as!( Users, "SELECT id, nama_siswa, username FROM public.users" )
    //         .fetch_all(pool) .await.unwrap()
    // }

    async fn users(&self, ctx: &Context<'_>) -> Vec<Users> {
        get_all(ctx.data::<PgPool>().expect("Can't get database connection"))
            .await
            .unwrap()
            .iter()
            .map(Users::from)
            .collect()
    }

    async fn get_user_by_id(&self, ctx: &Context<'_>, id: i32) -> Vec<Users> {
        get_by_id(id,ctx.data::<PgPool>().expect("Can't get database connection"))
            .await
            .unwrap()
            .iter()
            .map(Users::from)
            .collect()
    }
}

pub struct MutationRoot;

#[Object]
impl MutationRoot {

    async fn check(&self, _ctx: &Context<'_>) -> bool {
        true
    }

    async fn create_user(&self, ctx: &Context<'_>, users: UsersInput) -> Users {
        let new_user = NewUserEntity {
            id: users.id,
            nama_siswa: users.nama_siswa,
            username: users.username
        };

        let created_user_entity =
            insert(new_user, ctx.data::<PgPool>().expect("Can't get database connection"))
                .await
                .expect("Can't create user");

        Users::from(&created_user_entity)
    }

    async fn delete_by_nama_siswa(&self, ctx: &Context<'_>, nama_siswa: String) -> Result<String, Error> {

        let deleted = delete(nama_siswa, ctx.data::<PgPool>().expect("Can't get database connection")).await.unwrap();

        if deleted.rows_affected() > 0 {
            return Ok(String::from("users deleted"));
        }

        Err(Error::new("Can't delete a users"))
    }

    async fn delete_by_id(&self, ctx: &Context<'_>, id: i32) -> Result<String, Error> {

        let deleted = delete_by_id(id, ctx.data::<PgPool>().expect("Can't get database connection")).await.unwrap();

        if deleted.rows_affected() > 0 {
            return Ok(String::from("users deleted"));
        }

        Err(Error::new("Can't delete a users"))
    }

    async fn update(&self, ctx: &Context<'_>, user: UsersInput) -> Result<String, Error> {
        let updated = update(user.id, user.nama_siswa, user.username,ctx.data::<PgPool>()
            .expect("Can't get database connection"))
            .await
            .unwrap();

        if updated.rows_affected() > 0 {
            return Ok(String::from("updated"));
        }

        Err(Error::new("Can't update user"))
    }

    async fn update_data(&self, ctx: &Context<'_>, user: UsersInput) -> Result<String, Error> {
        let up_user = Users {
            id: user.id,
            nama_siswa: user.nama_siswa,
            username: user.username
        };

        let updated = update_data(up_user, ctx.data::<PgPool>()
            .expect("Can't get database connection"))
            .await
            .unwrap();

        if updated.rows_affected() > 0 {
            return Ok(String::from("updated"));
        }

        Err(Error::new("Can't update user"))
    }

}

// #[async_graphql::Object]
// impl Query {
//     /// Returns the sum of a and b
//     async fn add(&self, a: i32, b: i32) -> i32 {
//         a + b
//     }
// }

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(env!("DATABASE_URL")).await.unwrap();

    let create_schema = Schema::build(QueryRoot, MutationRoot, EmptySubscription)
        .enable_federation()
        .data(pool)
        .finish();

    let schema = web::Data::new(create_schema);

    println!("Playground: http://localhost:8080");

    HttpServer::new(move || {
        App::new()
            .app_data(schema.clone())
            .configure(configure_service)
        // .service(sum)
    })
        .bind("127.0.0.1:8080")?
        .run()
        .await.unwrap();

    Ok(())
}