use async_graphql::*;
use serde::{Deserialize, Serialize};
use sqlx::postgres::PgPool;
use uuid::Uuid;

use crate::{
    models::data_users::{NewUserModel, UserModel},
    models::santri::{NewSantriModel, SantriModel},
    models::person::{NewPersonModel, PersonModel},
    database::repository,
    database::get_person
};

#[derive(SimpleObject)]
struct Users {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}

impl From<&UserModel> for Users {
    fn from(model : &UserModel) -> Self {
        Users {
            id: model.id.clone(),
            nama_siswa: model.nama_siswa.clone(),
            username: model.username.clone()
        }
    }
}

#[derive(InputObject)]
pub struct UsersInput {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}

pub struct MutationRoot;

#[Object]
impl MutationRoot {
    async fn input_user(&self, ctx: &Context<'_>, user: UsersInput) -> Users {
        let new_user = NewUserModel {
            id: user.id,
            nama_siswa: user.nama_siswa,
            username: user.username
        };

        let input_user = repository::insert(new_user, ctx.data::<PgPool>()
            .expect("Can't get database connection"))
            .await
            .expect("Can't create user");

        Users::from(&input_user)
    }

    async fn update_user(&self, ctx: &Context<'_>, user: UsersInput) -> Result<String, Error> {

        let up_user = UserModel {
            id: user.id,
            nama_siswa: user.nama_siswa,
            username: user.username
        };

        let updated = repository::update_user(up_user, ctx.data::<PgPool>()
            .expect("Can't get database connection"))
            .await
            .unwrap();

        if updated.rows_affected() > 0 {
            return Ok(String::from("user telah diupdate"));
        }

        Err(Error::new("Can't update a users"))
    }

    async fn delete_by_id(&self, ctx: &Context<'_>, id: i32) -> Result<String, Error> {

        let deleted = repository::delete_by_id(id, ctx.data::<PgPool>()
            .expect("Can't get database connection"))
            .await
            .unwrap();

        if deleted.rows_affected() > 0 {
            return Ok(String::from("user telah dihapus"));
        }

        Err(Error::new("Can't delete a users"))
    }
}

