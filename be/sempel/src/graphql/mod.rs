pub mod query;
pub mod mutation;

use async_graphql::{EmptySubscription, Schema};

pub use mutation::MutationRoot;
pub use query::QueryRoot;

pub type AppSchema = Schema<QueryRoot, MutationRoot, EmptySubscription>;