use async_graphql::*;
use serde::{Deserialize, Serialize};
use sqlx::postgres::PgPool;
use uuid::Uuid;

use crate::{
    models::data_users::{NewUserModel, UserModel},
    models::santri::{NewSantriModel, SantriModel},
    models::person::{NewPersonModel, PersonModel},
    database::repository,
    database::get_person
};

#[derive(SimpleObject)]
struct Person {
    pub id: Uuid,
    pub nama_siswa: String,
    pub username: Option<String>,
    pub password: Option<String>
}

impl From<&PersonModel> for Person {
    fn from(model : &PersonModel) -> Self {
        Person {
            id: model.id.clone(),
            nama_siswa: model.nama_siswa.clone(),
            username: model.username.clone(),
            password: model.password.clone()
        }
    }
}

#[derive(SimpleObject)]
struct User {
    id: i32,
    nama_siswa: String,
    username: Option<String>
}

impl From<&UserModel> for User {
    fn from(model : &UserModel) -> Self {
        User {
            id: model.id.clone(),
            nama_siswa: model.nama_siswa.clone(),
            username: model.username.clone()
        }
    }
}

#[derive(SimpleObject)]
struct Santri {
    id: i32,
    nama_santri: String,
    password: String
}

impl From<&SantriModel> for Santri {
    fn from(model : &SantriModel) -> Self {
        Santri {
            id: model.id.clone(),
            nama_santri: model.nama_santri.clone(),
            password: model.password.clone()
        }
    }
}

pub struct QueryRoot;

#[Object]
impl QueryRoot {

    async fn get_all_person(&self, ctx: &Context<'_>) -> Vec<Person> {
        get_person::get_all(ctx.data::<PgPool>().expect("Can't get database connection"))
            .await
            .unwrap()
            .iter()
            .map(Person::from)
            .collect()
    }

    async fn get_users(&self, ctx: &Context<'_>) -> Vec<User> {
        repository::get_all(ctx.data::<PgPool>().expect("Can't get database connection"))
            .await
            .unwrap()
            .iter()
            .map(User::from)
            .collect()
    }

    async fn get_users_by_id(&self, ctx: &Context<'_>, id: i32) -> Vec<User> {
        repository::get_users_by_id(id,ctx.data::<PgPool>().expect("Can't get database connection"))
            .await
            .unwrap()
            .iter()
            .map(User::from)
            .collect()
    }

    async fn get_all_santri(&self, ctx: &Context<'_>) -> Vec<Santri> {
        repository::get_all_santri(ctx.data::<PgPool>().expect("Can't get database connection"))
            .await
            .unwrap()
            .iter()
            .map(Santri::from)
            .collect()
    }
}

