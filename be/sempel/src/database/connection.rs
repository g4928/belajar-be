use std::env;
use sqlx::postgres::{PgPool, PgPoolOptions};

pub async fn connection_db_pool() -> PgPool {
    let url = env::var("DATABASE_URL").expect("Can't get postgres db url");
    PgPoolOptions::new()
        .max_connections(10)
        .connect_timeout(std::time::Duration::from_secs(10))
        .connect(&url)
        .await
        .expect("Can't connect to database")
}