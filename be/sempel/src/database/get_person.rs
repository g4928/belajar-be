use sqlx::postgres::*;
use async_graphql::*;
use uuid::Uuid;
use crate::models::person::{NewPersonModel, PersonModel};

pub async fn get_all(pool: &PgPool) -> Result<Vec<PersonModel>, Error> {
    let person = sqlx::query_as!(PersonModel, "SELECT id, nama_siswa, username, password FROM person")
        .fetch_all(pool)
        .await?;

    Ok(person)
}

pub async fn insert_person(new: NewPersonModel, pool: &PgPool) -> Result<PersonModel, Error> {
    let users = sqlx::query!("INSERT INTO person (nama_siswa, username, password) VALUES ($1, $2, $3)
        RETURNING id, nama_siswa, username, password",
        new.nama_siswa, new.username, new.password )
        .fetch_one(pool)
        .await?;

    let input = PersonModel {
        id: users.id,
        nama_siswa: users.nama_siswa,
        username: users.username,
        password: users.password
    };

    Ok(input)
}