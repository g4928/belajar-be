use sqlx::postgres::*;
use async_graphql::*;
use crate::models::data_users::{NewUserModel, UserModel};
use crate::models::santri::{NewSantriModel, SantriModel};
// use sqlx::{postgres::PgPoolOptions, PgPool};

pub async fn get_all(pool: &PgPool) -> Result<Vec<UserModel>, Error> {
    let users = sqlx::query_as!(UserModel, "SELECT id, nama_siswa, username FROM users")
        .fetch_all(pool)
        .await?;

    Ok(users)
}

pub async fn get_users_by_id(id: i32, pool: &PgPool) -> Result<Vec<UserModel>, Error> {
    let users = sqlx::query_as!(UserModel, "SELECT id, nama_siswa, username FROM users WHERE id = $1",
    id )
        .fetch_all(pool)
        .await?;

    Ok(users)
}

pub async fn get_all_santri(pool: &PgPool) -> Result<Vec<SantriModel>, Error> {
    let santri = sqlx::query_as!(SantriModel, "SELECT id, nama_santri, password FROM santri")
        .fetch_all(pool)
        .await?;

    Ok(santri)
}

pub async fn insert(new: NewUserModel, pool: &PgPool) -> Result<UserModel, Error> {
    let users = sqlx::query!("INSERT INTO users (id, nama_siswa, username) VALUES ($1, $2, $3)
        RETURNING id, nama_siswa, username",
    new.id, new.nama_siswa, new.username )
        .fetch_one(pool)
        .await?;

    let input = UserModel {
        id: users.id,
        nama_siswa: users.nama_siswa,
        username: users.username
    };

    Ok(input)
}

pub async fn update_user(user: UserModel, pool: &PgPool) -> Result<PgQueryResult, sqlx::Error> {
    let del = sqlx::query!("UPDATE users SET nama_siswa = $1, username = $2 WHERE id = $3",
        user.nama_siswa, user.username, user.id)
        .execute(pool)
        .await?;

    Ok(del)
}

pub async fn delete_by_id(id: i32, pool: &PgPool) -> Result<PgQueryResult, sqlx::Error> {
    let del = sqlx::query!("DELETE FROM users WHERE id = $1", id)
        .execute(pool)
        .await?;

    Ok(del)
}