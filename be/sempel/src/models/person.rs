use async_graphql::SimpleObject;
use uuid::Uuid;

#[derive(SimpleObject)]
pub struct PersonModel {
    pub id: Uuid,
    pub nama_siswa: String,
    pub username: Option<String>,
    pub password: Option<String>
}

pub struct NewPersonModel {
    pub nama_siswa: String,
    pub username: Option<String>,
    pub password: Option<String>
}
