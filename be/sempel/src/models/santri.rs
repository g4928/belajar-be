use async_graphql::SimpleObject;

#[derive(SimpleObject)]
pub struct SantriModel {
    pub id: i32,
    pub nama_santri: String,
    pub password: String
}

pub struct NewSantriModel {
    pub id: i32,
    pub nama_santri: String,
    pub password: String
}
