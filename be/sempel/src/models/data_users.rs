use async_graphql::SimpleObject;

#[derive(SimpleObject)]
pub struct UserModel {
    pub id: i32,
    pub nama_siswa: String,
    pub username: Option<String>
}

pub struct NewUserModel {
    pub id: i32,
    pub nama_siswa: String,
    pub username: Option<String>
}
