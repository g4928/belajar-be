use actix_web::{ App, web, HttpServer, HttpRequest, HttpResponse, Result };
use async_graphql::*;
use async_graphql_actix_web::{GraphQLRequest, GraphQLResponse};
use async_graphql::http::{playground_source, GraphQLPlaygroundConfig};
use sqlx::{postgres::PgPoolOptions};
// use sqlx::postgres::PgQueryResult;

use crate::graphql::{AppSchema, MutationRoot, QueryRoot};
// use crate::database::connection::connection_db_pool;

pub mod database;
pub mod graphql;
pub mod models;


//-------------------------------------------------
fn configure_service(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/")
            .route(web::post().to(index))
            .route(web::get().to(index_playgroud))
    );
}


async fn index(
    schema: web::Data<AppSchema>,
    _http_req: HttpRequest,
    req: GraphQLRequest
) -> GraphQLResponse {
    let query = req.into_inner();

    schema.execute(query).await.into()

}

async fn index_playgroud() -> Result<HttpResponse> {
    let source = playground_source(GraphQLPlaygroundConfig::new("/").subscription_endpoint("/"));
    Ok(HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(source))
}



#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(env!("DATABASE_URL")).await.unwrap();

    let create_schema = Schema::build(QueryRoot, MutationRoot, EmptySubscription)
        .enable_federation()
        .data(pool)
        .finish();

    let schema = web::Data::new(create_schema);

    println!("Playground: http://localhost:8080");

    HttpServer::new(move || {
        App::new()
            .app_data(schema.clone())
            .configure(configure_service)
        // .service(sum)
    })
        .bind("127.0.0.1:8080")?
        .run()
        .await.unwrap();

    Ok(())
}