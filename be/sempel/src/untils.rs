use actix_web::{Result, HttpRequest};
use argonautica::{Error, Hasher, Verifier};
use lazy_static::lazy_static;
use chrono::{Duration, Local};
use serde::{Deserialize, Serialize};