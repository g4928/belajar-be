use sqlx::postgres::PgPoolOptions;
use dotenv::dotenv;
use std::env;

#[async_std::main]
async fn main() -> tide::Result<()> {
    dotenv().ok();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&env::var("DATABASE_URL")?).await?;

    let users = sqlx::query!(
        "SELECT id, nama_siswa, username, password FROM person")
        .fetch_all(&pool) .await?;

    println!("users: {:#?}", users);
    println!("selesai.");

    Ok(())
}

