extern crate chrono;

use std::io;
use std::io::Write;
// use chrono::{DateTime, Utc, Local};

fn main() {
    println!("=========================");
    println!("==== Selamat Datang  ====");
    // println!("=== input kelipatan 7 ===");
    println!("=========================");

    //no.1
    // println!("nama: Roypik");
    // println!("tahun: 2003");

    // println!("=========================");

    //no.2
    // print!("Nama       : ");
    // io::stdout().flush().unwrap();

    // let mut nama = String::new();
    // io::stdin()
    //     .read_line(&mut nama)
    //     .expect("failed to read line");
    
    // print!("Tahun Lahir: ");
    // io::stdout().flush().unwrap();
    
    // let mut tanggal = String::new();
    // io::stdin()
    //     .read_line(&mut tanggal)
    //     .expect("failed to read line");

    // println!("=========================");

    // let local_time = Local::now();
    // println!("local time is {}", local_time);

    let now = chrono::Utc::now();
    println!("Tanggal : {}", now.format("%Y %b %-d"));
    println!("=========================");

    print!("Nama          : ");
    io::stdout().flush().unwrap();

    let mut nama = String::new();
    io::stdin()
        .read_line(&mut nama)
        .expect("failed to read line");
    
    print!("Tahun Sekarang: ");
    io::stdout().flush().unwrap();
    
    let mut sekarang = String::new();
    io::stdin()
        .read_line(&mut sekarang)
        .expect("failed to read line");

        print!("Tahun Lahir   : ");
        io::stdout().flush().unwrap();
        
        let mut tanggal = String::new();
        io::stdin()
            .read_line(&mut tanggal)
            .expect("failed to read line");
    
    // let a = now.format("%Y").to_string();
    let b:i32 = sekarang.trim().parse().unwrap();
    let c:i32 = tanggal.trim().parse().unwrap();

    let umur = b - c;
    println!("Umur          : {}", umur);

    // tanggal
    let hasil_tgl = c % 2;

    // umur
    let hasil_umur = umur % 2;

    println!("=========================");
    if hasil_tgl == 0 || hasil_umur == 0 {
        println!("salah satu genap");
    } else if hasil_tgl != 0 && hasil_umur != 0 {
        println!("tahun dan umur anda ganjil untuk saat ini");            
    } else if hasil_tgl != 0 && hasil_umur == 0 {
        println!("tahun lahirmu anda ganjil yaitu {} dan umur anda genap yaitu {}", c, umur); 
    } else if hasil_umur != 0 && hasil_tgl == 0 {
        println!("tahun lahirmu anda genap yaitu {} dan umur anda ganjil yaitu {}", c, umur); 
    } else if hasil_tgl == 0 && hasil_umur == 0 {
        println!("tahun dan umur anda genap untuk saat ini");
    } 
    // else if hasil_umur == 0 {
    //     println!("umur anda genap untuk saat ini");            
    // } else  if hasil_tgl == 0 {
    //     println!("tahun lahirmu anda genap");            
    // } 
    else {
        println!("Not Found. Oops!!");
    };
    
    println!("=========================");

    // println!("umu_dan_tahun :{}", umu_dan_tahun);
    println!("hasil tgl :{}", hasil_tgl);
    println!("hasil umur :{}", hasil_umur);

    println!("=========================");

    println!("ini c :{}", c);
    // println!("Nama : {}", nama);
    // println!("Nama : {}", nama);
}
