#[derive(Debug)]
struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

fn main() {
    print!("helo");

    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    let user2 = User {
        email: "Roypik@gmail.com".to_string(),
        username: "Roypik6113".to_string(),
        active: true,
        sign_in_count: 1,
    };
    
    println!("{:#?},\n {:#?}", user1, user2)
}