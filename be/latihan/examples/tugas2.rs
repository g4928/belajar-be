extern crate chrono;

use std::io;
use std::io::Write;

fn main() {
    println!("=========================");
    println!("==== Selamat Datang  ====");
    println!("=========================");

    let now = chrono::Utc::now();
    println!("Tanggal : {}", now.format("%Y %b %-d"));

    println!("=========================");

    //no.1
    // println!("nama: Roypik");
    // println!("tahun: 2003");

    // println!("=========================");

    //no.2
    // print!("Nama       : ");
    // io::stdout().flush().unwrap();

    // let mut nama = String::new();
    // io::stdin()
    //     .read_line(&mut nama)
    //     .expect("failed to read line");

    // print!("Tahun Lahir: ");
    // io::stdout().flush().unwrap();
    
    // let mut tanggal = String::new();
    // io::stdin()
    //     .read_line(&mut tanggal)
    //     .expect("failed to read line");

    // let a = now.format("%Y").to_string();
    // let b:i32 = a.trim().parse().unwrap();
    // let c:i32 = tanggal.trim().parse().unwrap();
    // let umur = b - c;
    // println!("Umur          : {}", umur);

    // no.3
    print!("Nama       : ");
    io::stdout().flush().unwrap();

    let mut nama = String::new();
    io::stdin()
        .read_line(&mut nama)
        .expect("failed to read line");

    print!("Tahun Lahir: ");
    io::stdout().flush().unwrap();
    
    let mut tanggal_lahir = String::new();
    io::stdin()
        .read_line(&mut tanggal_lahir)
        .expect("failed to read line");

    // let a = now.format("%Y").to_string();
    // let b:i32 = a.trim().parse().unwrap();
    let c:i32 = tanggal_lahir.trim().parse().unwrap();
    // let umur = b - c;
    let mut n = c;

    while n < 2022 {
        if n == 2004 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2005 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2006 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2007 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2008 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2009 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2010 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2011 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2012 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2013 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        } else if n == 2014 {
            println!("pada tahun {} saya berumur {}", n, n - c);
        }

        // Increment counter
        n += 1;
    }
}