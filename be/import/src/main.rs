use std::env;
use tide::{Response, Body, Result};
use sqlx::postgres::PgPoolOptions;
use std::fs::File;
use std::io::prelude::*;
extern crate csv;


#[async_std::main]
async fn main() -> Result<()> {
    dotenv::dotenv().ok();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&env::var("DATABASE_URL")?).await?;

    let mut file = File::open("data/csv.csv").expect("can't open file !");

    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Oope! can't not read...");

    let mut rdr = csv::ReaderBuilder::new().from_reader(contents.as_bytes());
    for  result in rdr.records() {
        let  rec = result.expect("no result");

        let _fields : String = rec.iter().map(|f| f.to_string()).collect();


        let id = &rec[0].parse::<i32>().unwrap();
        let nama = &rec[1];

        println!("{}, {}", id, nama);

        sqlx::query!("INSERT INTO test (
            id, nama)
            VALUES ($1, $2)",
            id, nama)
            .execute(&pool) .await?;
    }
    // let mut wtr = csv::Writer::from_writer(io::stdout());

    //
    // for result in contents
    //
    //
    // println!("File contents:\n\n{}", contents);

    // let read = sqlx::query!(
    //     "SELECT id, nama_santri, password FROM santri")
    //     .fetch_all(&pool) .await?;
    //
    // println!("users: {:#?}", read);
    println!("finish");

    Ok(())
}
