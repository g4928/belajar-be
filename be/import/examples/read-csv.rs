use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut file = File::open("data/csv.csv").expect("can't open file !");

    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Oope! can't not read...");

    println!("File contents:\n\n{}", contents);
}