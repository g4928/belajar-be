use tide::Server;
use crate::api::*;
use sqlx::PgPool;

pub fn set(app: &mut Server<PgPool>) -> Result<(), std::io::Error> {
    app.at("/santri")
        .get(santri::get_santri)
        .post( santri::add)
        .patch( santri::update_santri);

    app.at("/santri/:id")
        .delete(santri::delete_santri)
        .get(santri::get_santri_by_id);

    // app.at("/person")
    //     .get(person::get_person);

    Ok(())
}