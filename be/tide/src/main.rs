use std::env;
use tide::{Response, Body};
use sqlx::postgres::PgPoolOptions;

mod paths;
mod api;

#[derive(serde::Serialize)]
struct WebServiceResponse {
    status: String,
    info: String,
}

fn ws_response(status_val: &str, info_val: &str) -> tide::Result<Response> {
    let mut res = Response::new(200);
    let data = WebServiceResponse {
        status: status_val.into(), info: info_val.into() };
    res.set_body(Body::from_json(&data)?);
    Ok(res)
}

fn to_json(data: impl serde::Serialize) -> tide::Result<Response> {
    let mut res = Response::new(200);
    res.set_body(Body::from_json(&data)?);
    Ok(res)
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    dotenv::dotenv().ok();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&env::var("DATABASE_URL")?).await?;

    tide::log::start();
    let mut app = tide::with_state(pool.clone());

    let _res = paths::set(&mut app);
    app.listen("0.0.0.0:8181").await?;
    Ok(())
}
