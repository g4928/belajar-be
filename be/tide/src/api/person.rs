// use tide::{Request, Response, Result};
// use sqlx::{Postgres, Pool, PgPool};
// use uuid::Uuid;
// use serde::{Serialize, Deserialize};
//
// #[derive(Serialize, Deserialize, Debug)]
// struct Person {
//     id: Uuid,
//     nama_siswa: Option<String>,
//     username: Option<String>,
//     password: Option<String>
// }
//
// pub async fn get_person(req: Request<PgPool>) -> Result<Response> {
//     let pool = req.state();
//
//     let person = sqlx::query_as!(Person,"SELECT id, nama_siswa, username, password from person")
//         .fetch_all(pool).await?;
//
//     crate::to_json(person)
// }
//
// // pub async fn get_santri_by_id( req: Request<PgPool>) -> Result<Response> {
// //     // let nama_santri = req.param("nama_santri")?.to_string();
// //     let id = req.param("id")?.parse::<Uuid>().unwrap();
// //
// //     let pool = req.state();
// //     let person = sqlx::query_as!(Person,"SELECT id, nama_siswa, username, password from person where id = $1", id)
// //         .fetch_all(pool).await?;
// //
// //     crate::to_json(person)
// // }
// //
// // pub async fn add(mut req: Request<PgPool>) -> Result<Response> {
// //     let x : Vec<Santri> = req.body_json().await?;
// //     let pool = req.state();
// //     for insert in x.iter() {
// //         let _santri_list = sqlx::query!(
// //             "Insert into santri (id, nama_santri, password) values ($1 , $2, $3)",
// //             insert.id, insert.nama_santri, insert.password)
// //             .execute(pool) .await?;
// //     }
// //     crate::ws_response("OK", "Data telah tersimpan")
// // }
// //
// // pub async fn update_santri(mut req: Request<PgPool>) -> Result<Response> {
// //     let x : Santri = req.body_json().await?;
// //     let pool = req.state();
// //     let _update_santri = sqlx::query!(
// //             "update santri set nama_santri = $1, password = $2 where id = $3",
// //              x.nama_santri, x.password, x.id)
// //         .execute(pool) .await?;
// //
// //     crate::ws_response("OK", "Data telah diupdate")
// // }
// //
// // pub async fn delete_santri( req: Request<Pool<Postgres>>) -> Result<Response> {
// //     let id = req.param("id")?.parse::<i32>().unwrap();
// //     // let x : Santri = req.body_json().await?;
// //     // let id: i32 = req.param("id")?.unwrap();
// //     let pool = req.state();
// //
// //     // let id = x[0].id;
// //     let _delete_santri = sqlx::query!("delete from santri where id = $1", id)
// //         .execute(pool) .await?;
// //
// //     crate::ws_response("OK", "Data telah didelete")
// // }
//
